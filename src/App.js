import { SafeAreaView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { Provider } from 'react-redux';
import { Store } from './store';
import Todos from './container/todos/index';


const styles = StyleSheet.create({
  root: {
    backgroundColor: '#263238',
    flex: 1,
    flexDirection: 'column'
  }
})

const App = () => {
  const backgroundStyle = {
    backgroundColor: '#ffffff',
  };
  return (
    <Provider store={Store}>
      <SafeAreaView style={styles.root}>
        <Todos />
      </SafeAreaView>
    </Provider>
  )
}

export default App