import { applyMiddleware, combineReducers, createStore } from "redux";
import ReduxThunk from 'redux-thunk';

import TodosReducer from '../Reducers';

const Reducers = {
    appData: TodosReducer
}

export const Store = createStore(combineReducers(Reducers), applyMiddleware(ReduxThunk))