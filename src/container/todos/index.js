import React from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    TextInput,
    ActivityIndicator,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

import { connect } from 'react-redux';

import styles from './styles';
import { getTodos } from '../../actions';

class Todos extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            title: '',
            description: '',
            selectedStatus: 'ON_PROGRESS',
            isLoading: true
        }
    }

    componentDidMount() {
        this.props.getTodoData()
        this.setState({ isLoading: false })
    }

    render() {
        console.log(this.props.data);
        return (
            <>
                {this.state.isLoading ? <ActivityIndicator /> :
                    (<View style={styles.root}>
                        <View style={styles.headingContainer}>
                            <Text style={styles.heading}>Redux Todo List</Text>
                        </View>

                        <View style={styles.inputContainer}>
                            <TextInput
                                style={styles.input}
                                onChangeText={(text) => this.setState({ title: text })}
                                value={this.state.title}
                                placeholder="Title"
                                placeholderTextColor="white"

                            />
                            <TextInput
                                style={styles.input}
                                onChangeText={(text) => this.setState({ description: text })}
                                value={this.state.description}
                                placeholder="Description"
                                placeholderTextColor="white"

                            />
                        </View>
                        <View style={styles.statusesContainer}>
                            <TouchableOpacity
                                onPress={() => this.setState({ selectedStatus: 'ON_PROGRESS' })}
                                style={[
                                    styles.statusButton,
                                    this.state.selectedStatus === 'ON_PROGRESS' && styles.statusButtonSelected
                                ]}
                            >
                                <Text
                                    style={[
                                        styles.statusButtonText,
                                        this.state.selectedStatus === 'ON_PROGRESS' && styles.statusButtonTextSelected
                                    ]}
                                >
                                    Progress
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => this.setState({ selectedStatus: 'DONE' })}
                                style={[
                                    styles.statusButton,
                                    this.state.selectedStatus === 'DONE' && styles.statusButtonSelected
                                ]}
                            >
                                <Text
                                    style={[
                                        styles.statusButtonText,
                                        this.state.selectedStatus === 'DONE' && styles.statusButtonTextSelected
                                    ]}
                                >
                                    Done
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', marginLeft: 'auto', marginTop: 20 }}>
                            <TouchableOpacity style={styles.saveButon}>
                                <Text style={styles.buttonText}>Save</Text>
                                <Icon name="save" size={20} color="white" />
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.cancelButton}>
                                <Text style={styles.buttonText}>Cancel</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={styles.content}>
                            <Text style={styles.todoDateText}>Monday</Text>
                            <ScrollView showsVerticalScrollIndicator>
                                {this.props.data.length > 0 && this.props.data.map(itemTodo => (
                                    <View style={styles.cardListContainer}>
                                        <View style={styles.todoCard}>
                                            <View style={styles.todoTitleContainer}>
                                                <View style={styles.todoActionContainer}>
                                                    <Text style={styles.todoTitle}>{itemTodo.title}</Text>
                                                    <TouchableOpacity style={styles.doneBadge}>
                                                        <Text style={styles.doneBadgeText}>{itemTodo.status}</Text>
                                                    </TouchableOpacity>
                                                </View>

                                                <View style={styles.todoActionContainer}>
                                                    <TouchableOpacity style={styles.editButton}>
                                                        <Icon name="edit" size={25} color="white" />
                                                    </TouchableOpacity>
                                                    <TouchableOpacity>
                                                        <Icon name="trash-2" size={25} color="white" />
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                            <View>
                                                <Text style={styles.todoDescription}>{itemTodo.title}</Text>
                                            </View>
                                        </View>
                                    </View>
                                ))}
                            </ScrollView>
                        </View>
                    </View>
                    )}
            </>
        )
    }
};
const mapStateToProps = (state) => {
    return {
        data: state.appData.todos
    }
};
const mapDispatchToProps = {
    getTodoData: getTodos
}

export default connect(mapStateToProps, mapDispatchToProps)(Todos);
